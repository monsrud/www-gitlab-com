---
layout: handbook-page-toc
title: "Conversica"
description: "This page is a dedicated resource for the Conversica tool."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

## About Conversica

Conversica is a Conversational AI tool that help enterprise marketing, sales, customer success, and finance teams attract, acquire and grow customers at scale across the customer revenue lifecycle. The AI Assitant works by engageing the prospect in a human like conversation over email in an effort to further qualify the prospect. 

In keeping with our GitLab value of Transparency, our AI assitant is named "Free Trial Bot" and it is named as a virtual assitant. 

## Conversica & Saleforce

We have installed a number of fields that will help you understand at what stage the prospect is at in Conversica


| Field Name | Description |
| ------ | ------ |
| Conversica Date Added | Date the lead was first added to Conversica |
| Conversica First Message Date | Date and time the lead was sent their first Conversica email |
| Conversica Last Message Date | Date and time the lead was sent their most recent Conversica email |
| Conversica Last Response Date | Date and time the lead last replied to Conversica |
| Conversation Stage | Defines the grouping of the lead stage, indicating where the lead is in a conversation. |
| Conversation Stage Date | Date and time the lead stage was last updated. |
| Conversation Status | Provides additional details on the messaging status of a lead. |
| Conversation Status Date | Date and time the Conversation Status was last updated. |
| Conversica Lead Status | Identifies the actionable label of the lead; for example, if they are a Hot Lead, Lead at Risk or if the response needs to be reviewed by a rep/manager. |
| Conversica Lead Status Date | Date and time the Lead Status was updated |
| Conversica Options | Ability to have Conversica follow-up after contact was made, or stop messaging. Details outlined below. |
| Conversica Hot Lead | Checked if the Lead/Contact is a Conversica Hot Lead |
| Conversica Hot Lead Date | Date/Time a Lead/Contact became a Conversica Hot Lead |
| Conversica Action Required | Checked if the Lead/Contact replied in a manner that Conversica has identified needs to be reviewed by a rep. Conversica will no longer message this lead unless they reply indicating interest. |
| Conversica Action Required Date | Date/Time a Lead/Contact was last flagged as Action Required |
| Conversica Lead At Risk | Checked if the Lead/Contact is a Conversica Lead At Risk |
| Conversica Lead At Risk Date | Date/Time a Lead/Contact became a Conversica Lead At Risk |
| Conversica Confirmed Phone 1 | If a new phone number is detected in a lead’s response, this field will be populated with the new number. |
| Conversica Options | This field will allow you to change statuses- you can select stop, which will stop all contact from the AI Assitant or Skip To Follow Up, where Converisca will wait and few days and then follow up with the prospect |

